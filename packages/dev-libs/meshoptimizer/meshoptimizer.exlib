# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=zeux tag=v${PV} ] cmake

SUMMARY="Mesh optimization library that makes meshes smaller and faster to render"
DESCRIPTION="
When a GPU renders triangle meshes, various stages of the GPU pipeline have to
process vertex and index data. The efficiency of these stages depends on the
data you feed to them; this library provides algorithms to help optimize
meshes for these stages, as well as algorithms to reduce the mesh complexity
and storage overhead.
The library provides a C and C++ interface for all algorithms; you can use it
from C/C++ or from other languages via FFI (such as P/Invoke)."

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DMESHOPT_BUILD_SHARED_LIBS:BOOL=TRUE
    -DMESHOPT_WERROR:BOOL=FALSE
    # Specifying soversion currently is at the guidance of the packager, so go
    # with 1 for the start
    -DMESHOPT_SOVERSION=1
)

