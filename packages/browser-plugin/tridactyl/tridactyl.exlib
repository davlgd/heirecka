# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github

export_exlib_phases src_prepare src_install pkg_postinst

SUMMARY="Native messaging host for the Tridactyl Firefox extension"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    run:
        dev-lang/python:*[>=3]
"

UPSTREAM_CHANGELOG="${HOMEPAGE}/blob/master/CHANGELOG.md"

tridactyl_src_prepare()  {
    edo sed \
        -e "s|REPLACE_ME_WITH_SED|/usr/$(exhost --target)/libexec/tridactyl/native_main.py|" \
        -i native/tridactyl.json
}

tridactyl_src_install() {
    exeinto /usr/$(exhost --target)/libexec/tridactyl
    doexe "${WORK}"/native/native_main.py

    insinto /usr/$(exhost --target)/lib/mozilla/native-messaging-hosts
    doins "${WORK}"/native/tridactyl.json
}

tridactyl_pkg_postinst() {
    elog "This package only installs a native messaging host. You also need to"
    elog "download the extension for your browser:"
    elog ""
    elog "https://addons.mozilla.org/de/firefox/addon/tridactyl-vim/"
}

