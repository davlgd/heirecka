# Copyright 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require udev-rules

export_exlib_phases src_prepare

SUMMARY="Tool for working with Logitech Unifying receivers and devices"
DESCRIPTION="
ltunify is a program resulting from the gathered knowledge on the Logitech
HID++ protocol. It allows you to pair additional devices like keyboards and
mice to your Unifying receiver, unpair existing devices and list information
about connected devices."

HOMEPAGE="https://lekensteyn.nl/logitech-unifying.html#ltunify"
DOWNLOADS="https://git.lekensteyn.nl/ltunify/snapshot/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_COMPILE_PARAMS=( ltunify )

DEFAULT_SRC_INSTALL_PARAMS=(
    bindir="/usr/$(exhost --target)/bin"
    udevrulesdir="${UDEVRULESDIR}"
)

ltunify_src_prepare() {
    default

    if [[ $(exhost --target) == *-musl* ]] ; then
        # musl doesn't have a libexecinfo and it appears unused. Also git
        # send-email'ed it to the author but the sed is a tad easier here.
        edo sed "/#include <execinfo.h>/d" -i ltunify.c
    fi
}

