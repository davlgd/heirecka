# Copyright 2018-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ unstable=true subdir=${PN} ] kde
require gtk-icon-cache

export_exlib_phases src_configure

SUMMARY="Simple viewer based on markdown formatted recipes"

LICENCES="
    CC0
    BSD-3 [[ note = [ cmake scripts ] ]]
    MIT"
SLOT="0"
MYOPTIONS=""

if ever at_least scm ; then
    MYOPTIONS+="
        ( providers: qt5 qt6 ) [[ number-selected = exactly-one ]]
    "

    QT_MIN_VER="[>=5.15.2]"
fi

DEPENDENCIES="
    suggestion:
        (
            app-editors/gedit
            app-editors/gvim
            kde/kate:4
        ) [[ *description = [ Editor to write and edit recipes ] ]]
"

if ever at_least scm ; then
    DEPENDENCIES+="
        build+run:
            providers:qt5? (
                x11-libs/qtbase:5${QT_MIN_VER}
                x11-libs/qtdeclarative:5${QT_MIN_VER}
            )
            providers:qt6? (
                x11-libs/qtbase:6${QT_MIN_VER}
                x11-libs/qtdeclarative:6${QT_MIN_VER}
            )
    "
else
    DEPENDENCIES+="
        build+run:
            app-text/discount:=
            x11-libs/qtbase:5${QT_MIN_VER}
            x11-libs/qtdeclarative:5${QT_MIN_VER}
    "
fi

kookbook_src_configure() {
    if ever at_least scm ; then
        if option providers:qt6 ; then
            ecmake $(kf6_shared_cmake_params)
        else
            ecmake $(kf5_shared_cmake_params)
        fi
    else
        kde_src_configure
    fi
}

# kookbooktouch is built but not installed
#    run:
#        kde/kirigami:5
#        x11-libs/qtquickcontrols:5

