# Copyright 2013-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.xz ] cmake
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_configure pkg_postinst pkg_postrm

SUMMARY="qBittorrent aims to provide a Free Software alternative to µtorrent"

LICENCES="GPL-2"
SLOT="0"

MYOPTIONS="
    headless [[ description = [ Build the headless qbittorrent-nox ] ]]
    webui [[ description = [ Enable the WebUI ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

QT6_MIN_VER="6.5.0"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.23]
        x11-libs/qttools:6[>=${QT6_MIN_VER}][?gui] [[ note = [ lrelease for translations ] ]]
    build+run:
        dev-libs/boost[>=1.76]
        net-p2p/libtorrent-rasterbar[>=2.0.10]
        sys-libs/zlib[>=1.2.11]
        x11-libs/qtbase:6[>=${QT6_MIN_VER}][?gui][sql]
        !headless? (
            x11-libs/qtbase:6[>=${QT6_MIN_VER}]
            x11-libs/qtsvg:6[>=${QT6_MIN_VER}]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=3.0.2] )
    run:
        dev-lang/python:*[>=3.3]    [[ note = [ needed by the search engine ] ]]
"

qbittorrent_src_configure() {
    local myconf=(
        $(cmake_option WEBUI)
        $(cmake_option !headless GUI)
        $(cmake_option headless SYSTEMD)
    )

    if [[ $(exhost --target) == *-musl* ]] ; then
        myconf+=( -DSTACKTRACE:BOOL=FALSE )
    fi

    ecmake "${myconf[@]}"
}

qbittorrent_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

qbittorrent_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

